#include <stdio.h>
#include <string.h>

int main(int argc, char *argv[])
{
	char character;
	char newline = 'n';

	// Open file
	FILE *f_out = fopen("teststring.in", "wb+");

	// Variables for string test
	character = 's';
	unsigned char n = 2;
	char *message[] = { "Holly", "Hardin", "CIS407" };
	unsigned char n0 = strlen(message[0]) - 1;
	unsigned char n1 = strlen(message[1]) - 1;
	unsigned char n2 = strlen(message[2]) - 1;

	// Write to file
	fwrite(&character, sizeof(char), 1, f_out);
	fwrite(&n, sizeof(unsigned char), 1, f_out);
	fwrite(&n0, sizeof(unsigned char), 1, f_out);
	fwrite(message[0], sizeof(char), strlen(message[0]), f_out);
	fwrite(&n1, sizeof(unsigned char), 1, f_out);
	fwrite(message[1], sizeof(char), strlen(message[1]), f_out);
	fwrite(&n2, sizeof(unsigned char), 1, f_out);
	fwrite(message[2], sizeof(char), strlen(message[2]), f_out);
	fwrite(&newline, sizeof(char), 1, f_out);

	// Close file
	fclose(f_out);

	// Open different file
	f_out = fopen("testdouble.in", "wb+");

	// Variables for double test
	character = 'd';
	double double0 = .407;
	double double1 = 4.07;
	double double2 = 40.7;

	// Write to file
	fwrite(&character, sizeof(char), 1, f_out);
	fwrite(&n, sizeof(unsigned char), 1, f_out);
	fwrite(&double0, sizeof(unsigned char), sizeof(double), f_out);
	fwrite(&double1, sizeof(unsigned char), sizeof(double), f_out);
	fwrite(&double2, sizeof(unsigned char), sizeof(double), f_out);
	fwrite(&newline, sizeof(char), 1, f_out);

	// Close file
	fclose(f_out);

	// Open different file
	f_out = fopen("testinteger.in", "wb+");

	// Variables for integer test
	character = 'i';
	int integer0 = 4;
	int integer1 = 0;
	int integer2 = 7;

	// Write to file
	fwrite(&character, sizeof(char), 1, f_out);
	fwrite(&n, sizeof(unsigned char), 1, f_out);
	fwrite(&integer0, sizeof(unsigned char), sizeof(int), f_out);
	fwrite(&integer1, sizeof(unsigned char), sizeof(int), f_out);
	fwrite(&integer2, sizeof(unsigned char), sizeof(int), f_out);
	fwrite(&newline, sizeof(char), 1, f_out);

	// Close file
	fclose(f_out);

	return 0;
}
