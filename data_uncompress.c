#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
	// Character variable
	char c;

	// Iterator variables
	int i;
	int j;

	// Numerical variables
	int n;
	int size;

	// Buffers
	char ch;
	int int_buffer;
	double double_buffer;

	// "Boolean" variable
	int needs_space = 0;

	// Read a character until end of file
	while ((c = getchar()) != EOF && c != '\n')
	{
		// If the character is n, then print a new line
		if (c == 'n')
		{
			printf("\n");
			needs_space = 0;
		}

		// If the character is s, then read n strings
		else if (c == 's')
		{
			// Get the number of strings to read
			n = getchar();

			// If the input is bigger than 255, then the input is not correct
			if (n > 255)
				fprintf(stderr, "Input error: no size\n");

			// If the last character printed was not a space, print a space
			if (needs_space == 1)
				printf(" ");

			// Read the number of strings + 1
			for (i = 0; i < n + 1; i++)
			{
				// Get the number of characters to read
				size = getchar();

				// If the size is bigger than 255, then the input is not correct
				if (size > 255)
					fprintf(stderr, "Input error: no string size\n");

				// Read the number of characters + 1
				for (j = 0; j < size + 1; j++)
				{
					// Read and print the character
					ch = getchar();
					printf("%c", ch);
					needs_space = 1;
				}

				// Print a space inbetween words
				if (i < n)
				{
					printf(" ");
					needs_space = 0;
				}
			}
		}

		// If the character is i, then read n integers
		else if (c == 'i')
		{
			// Get the number of integers to read
			n = getchar();

			// If the input is bigger than 255, then the input is not correct
			if (n > 255)
				fprintf(stderr, "Input error: no size\n");

			// If the last character printed was not a space, print a space
			if (needs_space == 1)
				printf(" ");

			// Read the number of integers + 1
			for (i = 0; i < n + 1; i++)
			{
				// Read and print the integer
				fread(&int_buffer, sizeof(int), 1, stdin);
				printf("%d", int_buffer);
				needs_space = 1;

				// Print a space inbetween integers
				if (i < n)
				{
					printf(" ");
					needs_space = 0;
				}
			}
		}

		// If the character is d, then read n doubles
		else if (c == 'd')
		{
			// Get the number of doubles to read
			n = getchar();

			// If the input is bigger than 255, then the input is not correct
			if (n > 255)
				fprintf(stderr, "Input error: no size\n");

			// If the last character printed was not a space, print a space
			if (needs_space == 1)
				printf(" ");

			// Read the number of doubles + 1
			for (i = 0; i < n + 1; i++)
			{
				// Read and print the double
				fread(&double_buffer, sizeof(unsigned char), sizeof(double), stdin);
				printf("%.10lg", double_buffer);
				needs_space = 1;

				// Print a space inbetween doubles
				if (i < n)
				{
					printf(" ");
					needs_space = 0;
				}
			}
		}

		// If the character is not n, s, i, or d, then the input isn't correct
		else if ((c != 'n') || (c != 's') || (c != 'i') || (c != 'd'))
		{
			fprintf(stderr, "Input error: bad type\n");
		}
	}
}

